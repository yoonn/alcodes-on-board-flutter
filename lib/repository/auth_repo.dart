import 'package:alcodes_on_board_flutter/constants/api_urls.dart';
import 'package:alcodes_on_board_flutter/models/response_models/sign_in_response_model.dart';
import 'package:alcodes_on_board_flutter/utils/api_components/api_response.dart';
import 'package:dio/dio.dart';
import 'package:meta/meta.dart';

class AuthRepo {
  Future<ApiResponse<SignInResponseModel>> signInAsync({
    @required String email,
    @required String password,
  }) async {
    try {
      final requestBody = {
        'email': email,
        'password': password,
      };

      final dio = Dio();
      dio.options.baseUrl = ApiUrls.baseUrl;

      final response = await dio.post(ApiUrls.login, data: requestBody);

      final data = SignInResponseModel(
        token: response.data['token'],
      );

      return ApiResponse<SignInResponseModel>(
        message: 'OK',
        data: data,
      );
    } on DioError catch (ex) {
      if (ex.response.statusCode == 400) {
        return Future.error(ex.response.data['error']);
      } else {
        return Future.error(ex);
      }
    } catch (ex) {
      return Future.error(ex);
    }
  }
}


