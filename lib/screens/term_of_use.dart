import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:webview_flutter/webview_flutter.dart';

class TermOfUse extends StatefulWidget {
  @override
  _TermOfUseState createState() => _TermOfUseState();
}

class _TermOfUseState extends State<TermOfUse>{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Term of Use'),
      ),
      body: const WebView(
        initialUrl:
        'https://okfn.org/terms-of-use/',
        javascriptMode: JavascriptMode.unrestricted,
      ),
    );
  }
}