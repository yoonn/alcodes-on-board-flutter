import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class MyFriendDetails extends StatefulWidget {
  var fname;
  var email;
  var lname;
  var avatar;
  MyFriendDetails(this.email, this.fname, this.lname, this.avatar);

  @override
  _MyFriendDetailsState createState() => _MyFriendDetailsState();
}

class _MyFriendDetailsState extends State<MyFriendDetails> {

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text('Friend Details'),
      ),
      body: ListView(
        children: <Widget>[
          head(),
          Divider(
            thickness: 0.8,
          ),
          otherDetails("First Name", '${widget.fname}'),
          otherDetails("Last Name", '${widget.lname}'),
          Divider(
            thickness: 0.8,
          )
        ],
      ),
    );
  }

  Widget head(){
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Center(
            child: CircleAvatar(
              radius: 50,
              backgroundImage: NetworkImage('${widget.avatar}'),
            ),
          ),
          Text(
            "Email",
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
          ),
          SizedBox(
            height: 10,
          ),
          Text('${widget.email}')
        ],
      ),);
  }

  Widget otherDetails(String label, String value){
    return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              "$label :",
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              value,
              style: TextStyle(fontSize: 16),
            )
          ],
        ));
  }
}




