import 'package:alcodes_on_board_flutter/screens/my_friend_details.dart';
import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'dart:async';
import 'package:alcodes_on_board_flutter/constants/api_urls.dart';

/// TODO Issue on this screen:
/// - Show list of my friends.
/// - Click list item go to my friend detail page.

class MyFriendList extends StatefulWidget {
  @override
  _MyFriendListState createState() => _MyFriendListState();
}

class _MyFriendListState extends State<MyFriendList> {

  List _users = [];


  Future <void> getUsers() async {
      final dio = Dio();
  dio.options.baseUrl = ApiUrls.baseUrl;

  var response = await dio.get(ApiUrls.listUser);
  Map userData = response.data;
  setState(() {
    _users = userData["data"];
  });


  return _users;
}

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('My Friend List'),
        ),
        body: Container(
            child: FutureBuilder(
                future: getUsers(),
                builder: (BuildContext context, AsyncSnapshot snapshot){
                  if(snapshot.data == null){
                    return Container(
                      child: Center(
                        child: Text('Loading'),
                      ),
                    );
                  }else{

                    return ListView.builder(
                      itemCount: _users.length,
                      itemBuilder: (context, index) {
                        return ListTile(
                          leading: CircleAvatar(
                              backgroundImage: NetworkImage(_users[index]['avatar'])),
                          title: Text(_users[index]['first_name'] +
                              " " +
                              _users[index]['last_name']),
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => MyFriendDetails(_users[index]['email'].toString(),_users[index]['first_name'].toString(),_users[index]['last_name'].toString(),_users[index]['avatar'].toString()),
                              ),
                            );
                          },
                        );
                      },
                    );}
                } )));
  }
}



