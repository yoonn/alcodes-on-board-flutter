import 'package:alcodes_on_board_flutter/app_router.dart';
import 'package:alcodes_on_board_flutter/constants/app_constants/app_constants.dart' as appConst;
import 'package:flutter/material.dart';

class RootApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
      return GestureDetector(
      onTap: () {
        // Tap anywhere to dismiss keyboard.
        // AppFocusHelper.instance.requestUnfocus();
      },
      child: MaterialApp(
        title: 'Alcodes OnBoard Flutter',
        theme: ThemeData(
          scaffoldBackgroundColor: appConst.kScreenBackgroundColor,
          primarySwatch: appConst.kPrimaryColor,
          accentColor: appConst.kAccentColor,
          textTheme: Theme.of(context).textTheme.apply(bodyColor: appConst.kTextColor),
          visualDensity: VisualDensity.adaptivePlatformDensity,
          appBarTheme: Theme.of(context).appBarTheme.copyWith(
                color: appConst.kAppBarBackgroundColor,
                textTheme: Theme.of(context).textTheme.apply(bodyColor: appConst.kAppBarTextColor),
                iconTheme: Theme.of(context).iconTheme.copyWith(color: appConst.kAppBarTextColor),
              ),
        ),
        initialRoute: AppRouter.splashScreen,
        onGenerateRoute: AppRouter.generatedRoute,
      ),
    );

}
}
